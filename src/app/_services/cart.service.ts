import { Injectable } from '@angular/core';
import { ProductModel } from '../_models/product';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class CartService {
	private products: ProductModel[] = [];
	private subj: BehaviorSubject<ProductModel[]> = new BehaviorSubject<ProductModel[]>([]);

	constructor() { }

	addToCart(product: ProductModel) {
		let existIndex = this.checkExists(product.title);
		if (existIndex != -1) {
			this.increment(existIndex);
		} else {
			product.cartCount++;
			this.products.push(product)
		}

		this.subj.next(this.products);
	}

	increment(index: number) {
		this.products[index].cartCount++;
	}

	decrement(index: number) {
		if (this.products[index].cartCount > 0) {
			this.products[index].cartCount--;
		}
	}

	checkExists(name: string) {
		return this.products.findIndex(p => p.title === name)
	}

	getCartProducts(): Observable<ProductModel[]> {
		return this.subj.asObservable();
	}
}
