import { Injectable } from '@angular/core';
import { ProductModel } from '../_models/product';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class StoreService {
	private storeProducts: ProductModel[] = [];
	private subj: BehaviorSubject<ProductModel[]> = new BehaviorSubject<ProductModel[]>([]);

	constructor() { }

	generateProducts(count: number) {
		for (let i = 0; i < count; i++) {
			let newProduct = new ProductModel(i);
			this.storeProducts.push(newProduct)
		}
		this.next();
	}

	next() {
		this.subj.next(this.storeProducts)
	}

	incrementQuantity(id: number) {
		this.storeProducts.map(p => {
			if(p.id == id){
				p.quantity++
			}
		});
		this.next();
	}

	decrementQuantity(id: number) {
		this.storeProducts.map(p => {
			if(p.id == id){
				p.quantity--
			}
		});
		this.next();
	}

	updatePrice(products: ProductModel[]){
		this.storeProducts = products;
		this.next();
	}

	getProducts(): Observable<ProductModel[]> {
		return this.subj.asObservable();
	}
}
