import { Component, OnInit } from '@angular/core';
import { StoreService } from './_services/store.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
	constructor(private storeService: StoreService){}

	ngOnInit(){
		this.generateProducts()
	}

	generateProducts(){
		this.storeService.generateProducts(10);
	}
}
