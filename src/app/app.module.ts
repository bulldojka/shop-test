import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { MatToolbarModule, MatButtonModule, MatInputModule, MatFormFieldModule } from '@angular/material';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CartComponent } from './cart/cart.component';
import { ShopComponent } from './shop/shop.component';
import { WarehouseComponent } from './warehouse/warehouse.component';
import { StoreService } from './_services/store.service';
import { CartService } from './_services/cart.service';
import { FormsModule } from '@angular/forms';

@NgModule({
	declarations: [
		AppComponent,
		CartComponent,
		ShopComponent,
		WarehouseComponent
	],
	imports: [
		FormsModule,
		MatToolbarModule,
		MatButtonModule,
		MatInputModule,
		MatFormFieldModule,
		BrowserModule,
		AppRoutingModule,
		BrowserAnimationsModule
	],
	providers: [StoreService, CartService],
	bootstrap: [AppComponent]
})
export class AppModule { }
