import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { CartService } from '../_services/cart.service';
import { ProductModel } from '../_models/product';
import { StoreService } from '../_services/store.service';

@Component({
	selector: 'cart',
	templateUrl: './cart.component.html',
	styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
	isShop: boolean = true;
	isOpen: boolean = false;
	products: ProductModel[] = [];

	constructor(private router: Router, private cartService: CartService, private storeService: StoreService) { }

	ngOnInit() {
		this.router.events.subscribe(
			e => {
				if (e instanceof NavigationEnd) {
					this.isShop = e.url === '/shop'
				}
			}
		)
		this.getCartProducts();
	}

	getCartProducts() {
		this.cartService.getCartProducts().subscribe(
			products => {
				this.products = products
			}
		)
	}

	add(index: number) {
		this.cartService.increment(index);
		this.storeService.decrementQuantity(this.products[index].id)
	}

	remove(index: number) {
		this.cartService.decrement(index);
		this.storeService.incrementQuantity(this.products[index].id)
	}

	get cartTotalCount(){
		let sum = 0
		if (this.products.length) {
			sum = this.products.reduce((acc, cur) => acc + cur.cartCount, 0)
		}
		return sum
	}

	get cartTotalPrice() {
		let sum = 0
		if (this.products.length) {
			sum = this.products.reduce((acc, cur) => acc + cur.price * cur.cartCount, 0)
		}
		return sum
	}

}
