import { Component, OnInit } from '@angular/core';
import { StoreService } from '../_services/store.service';
import { ProductModel } from '../_models/product';
import { CartService } from '../_services/cart.service';

@Component({
	selector: 'app-shop',
	templateUrl: './shop.component.html',
	styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit {
	products: ProductModel[] = [];

	constructor(private storeService: StoreService, private cartService: CartService) { }

	ngOnInit() {
		this.getStoreProducts();
	}

	getStoreProducts() {
		this.storeService.getProducts().subscribe(
			products => {
				this.products = products
			}
		)
	}

	addToCard(i: number) {
		this.storeService.decrementQuantity(this.products[i].id);
		this.cartService.addToCart(this.products[i])
	}

}
