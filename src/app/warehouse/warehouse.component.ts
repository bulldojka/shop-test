import { Component, OnInit } from '@angular/core';
import { StoreService } from '../_services/store.service';
import { ProductModel } from '../_models/product';

@Component({
	selector: 'app-warehouse',
	templateUrl: './warehouse.component.html',
	styleUrls: ['./warehouse.component.scss']
})
export class WarehouseComponent implements OnInit {
	products: ProductModel[] = [];

	constructor(private storeService: StoreService) { }

	ngOnInit() {
		this.getStoreProducts();
	}

	getStoreProducts() {
		this.storeService.getProducts().subscribe(
			products => this.products = products
		)
	}

}
