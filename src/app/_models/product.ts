export class ProductModel {
	id: number
	title: string;
	price: number;
	quantity: number;
	cartCount: number;

	constructor(id: number) {
		this.id = id;
		this.title = this.getTitle();
		this.price = Math.round(Math.random() * 100000)
		this.quantity = Math.floor(Math.random() * 10);
		this.cartCount = 0;
	}

	getTitle() {
		let titles = [
			'Ferrari Pininfarina Sergio',
			'Bugatti Veyron Vivere By Mansory',
			'Lykan Hypersport',
			'McLaren P1 LM',
			'Aston Martin Valkyrie',
			'Lamborghini Veneno',
			'Koenigsegg CCXR Trevita',
			'Mercedes- Benz Maybach Exelero'
		]

		return titles[Math.floor(Math.random() * titles.length)]
	}
}
